#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate lazy_static;
#[cfg(target_arch = "wasm32")]
#[cfg(test)]
#[macro_use]
extern crate wasm_bindgen_test;

pub mod filters;

mod content_types;
mod js_helper;
mod url;
mod utils;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub fn init() {
    utils::set_panic_hook();
}
