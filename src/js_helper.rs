#[cfg(target_arch = "wasm32")]
use js_sys as js;
use std::collections::HashMap;
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

/// Retrieve the index JS property value as a string
pub fn get_string_property_u32(value: &JsValue, index: u32) -> Option<String> {
    js::Reflect::get_u32(value, index)
        .ok()
        .and_then(|v| v.as_string())
}

/// Retrieve the named JS property value as an integer
pub fn get_int_property_string(value: &JsValue, name: &str) -> Option<i32> {
    js::Reflect::get(value, &JsValue::from(name))
        .ok()
        .and_then(|v| v.as_f64().map(|v| v as i32))
}

///
/// Insert a pair `JsValue` into a string HashMap
///
/// TODO: return a Result
pub fn insert_pair(hash: &mut HashMap<String, String>, pair: &JsValue) {
    if pair.is_object() {
        if let Some(k) = get_string_property_u32(pair, 0) {
            if let Some(v) = get_string_property_u32(pair, 1) {
                hash.insert(k, v);
            }
        }
    }
}

///
/// Push a pair `JsValue` into a vec of (string, string)
///
/// TODO: return a Result
pub fn push_pair(vec: &mut Vec<(String, String)>, pair: &JsValue) {
    if pair.is_object() {
        if let Some(k) = get_string_property_u32(pair, 0) {
            if let Some(v) = get_string_property_u32(pair, 1) {
                vec.push((k, v));
            }
        }
    }
}

/// Replace the occurences matching with RegExp re in source by replacement
pub fn replace_regexp(source: &str, re: &js::RegExp, replacement: &str) -> String {
    String::from(js::JsString::from(source).replace_by_pattern(&re, replacement))
}
