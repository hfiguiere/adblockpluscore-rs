use std::str::FromStr;

bitflags! {
    pub struct ContentTypes: u32 {
        // Types of web resources.
        const OTHER = 1;
        const SCRIPT = 2;
        const IMAGE = 4;
        const STYLESHEET = 8;
        const OBJECT = 16;
        const SUBDOCUMENT = 32;
        const WEBSOCKET = 128;
        const WEBRTC = 256;
        const PING = 1024;
        const XMLHTTPREQUEST = 2048; // 1 << 11

        const MEDIA = 16384;
        const FONT = 32768; // 1 << 15

        // Special filter options.
        const POPUP = 1 << 24;
        const CSP = 1 << 25;

        // Whitelisting flags.
        const DOCUMENT = 1 << 26;
        const GENERICBLOCK = 1 << 27;
        const ELEMHIDE = 1 << 28;
        const GENERICHIDE = 1 << 29;

        const BACKGROUND = Self::IMAGE.bits;
        const XBL = Self::OTHER.bits;
        const DTD = Self::OTHER.bits;
        const RESOURCE_TYPES = (1 << 24) - 1;
    }
}

pub enum ContentTypesParseError {
    Invalid,
}

impl FromStr for ContentTypes {
    type Err = ContentTypesParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "OTHER" => Ok(Self::OTHER),
            "SCRIPT" => Ok(Self::SCRIPT),
            "IMAGE" => Ok(Self::IMAGE),
            "STYLESHEET" => Ok(Self::STYLESHEET),
            "OBJECT" => Ok(Self::OBJECT),
            "SUBDOCUMENT" => Ok(Self::SUBDOCUMENT),
            "WEBSOCKET" => Ok(Self::WEBSOCKET),
            "WEBRTC" => Ok(Self::WEBRTC),
            "PING" => Ok(Self::PING),
            "XMLHTTPREQUEST" => Ok(Self::XMLHTTPREQUEST),
            "MEDIA" => Ok(Self::MEDIA),
            "FONT" => Ok(Self::FONT),
            "POPUP" => Ok(Self::POPUP),
            "CSP" => Ok(Self::CSP),
            "DOCUMENT" => Ok(Self::DOCUMENT),
            "GENERICBLOCK" => Ok(Self::GENERICBLOCK),
            "ELEMHIDE" => Ok(Self::ELEMHIDE),
            "GENERICHIDE" => Ok(Self::GENERICHIDE),
            "BACKGROUND" => Ok(Self::BACKGROUND),
            "XBL" => Ok(Self::XBL),
            _ => Err(ContentTypesParseError::Invalid),
        }
    }
}
