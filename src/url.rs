use std::collections::HashMap;
use std::str::Split;
use std::sync::Mutex;

/// Encode the domain source and the separator
#[derive(Debug)]
pub enum DomainSource {
    Pipe(String),
    Comma(String),
}

impl DomainSource {
    pub fn is_empty(&self) -> bool {
        match self {
            DomainSource::Pipe(s) | DomainSource::Comma(s) => s.is_empty(),
        }
    }

    /// Perform str::to_lowercase() on the enum content
    pub fn to_lowercase(&self) -> DomainSource {
        match self {
            DomainSource::Pipe(s) => DomainSource::Pipe(s.to_lowercase()),
            DomainSource::Comma(s) => DomainSource::Comma(s.to_lowercase()),
        }
    }

    /// Provide the separator
    pub fn separator(&self) -> char {
        match self {
            DomainSource::Pipe(_) => '|',
            DomainSource::Comma(_) => ',',
        }
    }

    pub fn as_str(&self) -> &str {
        match self {
            DomainSource::Pipe(s) | DomainSource::Comma(s) => &s,
        }
    }

    pub fn split<'a>(&'a self) -> Split<'a, char> {
        match self {
            DomainSource::Pipe(s) => s.split('|'),
            DomainSource::Comma(s) => s.split(','),
        }
    }

    /// Convert the domain string to a string
    /// This basically extract the string out of the enum.
    fn to_string(&self) -> String {
        match self {
            DomainSource::Pipe(s) | DomainSource::Comma(s) => s.to_string(),
        }
    }
}

impl std::fmt::Display for DomainSource {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

pub type DomainsInclude = HashMap<String, bool>;

lazy_static! {
    static ref DOMAINS_CACHE: Mutex<HashMap<String, DomainsInclude>> =
        Mutex::new(HashMap::with_capacity(1000));
}

pub fn parse_domains(source: &DomainSource) -> Option<DomainsInclude> {
    let source_str = source.as_str();
    let domains = DOMAINS_CACHE.lock().unwrap().get(source_str).cloned();
    if domains.is_some() {
        return domains;
    }

    let separator = source.separator();

    let domains: Option<DomainsInclude> =
        if !source_str.starts_with('~') && !source_str.contains(separator) {
            let mut domains: DomainsInclude = HashMap::new();
            domains.insert("".to_string(), false);
            domains.insert(source_str.to_string(), true);
            Some(domains)
        } else {
            let mut domains: Option<DomainsInclude> = None;
            let mut has_includes = false;
            for mut domain in source.split() {
                if domain.is_empty() {
                    continue;
                }
                let include = if domain.starts_with('~') {
                    domain = domain.get(1..).unwrap();
                    false
                } else {
                    has_includes = true;
                    true
                };

                if domains.is_none() {
                    domains = Some(HashMap::new());
                }

                domains
                    .as_mut()
                    .unwrap()
                    .insert(domain.to_string(), include);
            }

            if domains.is_some() {
                domains
                    .as_mut()
                    .unwrap()
                    .insert("".to_string(), !has_includes);
            }
            domains
        };

    DOMAINS_CACHE
        .lock()
        .unwrap()
        .insert(source.to_string(), domains.as_ref().unwrap().clone());
    domains
}
