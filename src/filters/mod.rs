mod base;
mod content;
mod regexp;

use js_sys as js;
use std::sync::Arc;

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

use base::{
    ActiveFilter, CommentFilter, FilterBase, FilterExt, InvalidFilter, ObjProps, Serialize, Text,
};

use regexp::{BlockingFilter, Pattern, RegExpFilter, WhitelistFilter};

use content::{ContentFilter, ElemHideExt, SnippetExt};

#[derive(Clone, Debug)]
pub enum FilterEnum {
    Invalid(Arc<InvalidFilter>),
    Comment(Arc<CommentFilter>),
    Whitelist(Arc<WhitelistFilter>),
    Blocking(Arc<BlockingFilter>),
    ElemHide(Arc<ContentFilter>),
    ElemHideException(Arc<ContentFilter>),
    ElemHideEmulation(Arc<ContentFilter>),
    Snippet(Arc<ContentFilter>),
}

impl FilterEnum {
    fn as_regexp_filter(&self) -> Option<&RegExpFilter> {
        use FilterEnum::*;

        match self {
            Whitelist(ref filter) => Some(filter.as_ref().as_ref()),
            Blocking(ref filter) => Some(filter.as_ref().as_ref()),
            _ => None,
        }
    }

    fn as_active_filter(&self) -> Option<&ActiveFilter> {
        use FilterEnum::*;
        match self {
            Whitelist(ref filter) => Some(filter.as_ref().as_ref()),
            Blocking(ref filter) => Some(filter.as_ref().as_ref()),
            ElemHide(ref filter)
            | ElemHideEmulation(ref filter)
            | ElemHideException(ref filter)
            | Snippet(ref filter) => Some(filter.as_ref().as_ref()),
            _ => None,
        }
    }
}

/// Filter is the exported object to JS
#[wasm_bindgen]
#[derive(Clone)]
pub struct Filter(FilterEnum);

#[wasm_bindgen]
impl Filter {
    #[wasm_bindgen(js_name = fromText)]
    pub fn from_text(text: &str) -> Self {
        crate::init();

        FilterBase::from_text(text, None)
    }

    #[wasm_bindgen(js_name = type, getter = type)]
    pub fn get_type(&self) -> Option<String> {
        use FilterEnum::*;
        match &self.0 {
            Comment(filter) => Some(filter.get_type().to_string()),
            Invalid(filter) => Some(filter.get_type().to_string()),
            Blocking(filter) => Some(filter.get_type().to_string()),
            Whitelist(filter) => Some(filter.get_type().to_string()),
            ElemHide(filter) => Some(filter.get_type().to_string()),
            ElemHideException(filter) => Some(filter.get_type().to_string()),
            ElemHideEmulation(filter) => Some(filter.get_type().to_string()),
            Snippet(filter) => Some(filter.get_type().to_string()),
        }
    }

    #[wasm_bindgen(js_name = isInstanceOf)]
    pub fn is_instance_of(&self, t: &str) -> bool {
        use FilterEnum::*;
        match t {
            "InvalidFilter" => {
                if let Invalid(_) = self.0 {
                    true
                } else {
                    false
                }
            }
            "CommentFilter" => {
                if let Comment(_) = self.0 {
                    true
                } else {
                    false
                }
            }
            "ActiveFilter" => match self.0 {
                Blocking(_) | Whitelist(_) | ElemHide(_) | ElemHideException(_)
                | ElemHideEmulation(_) | Snippet(_) => true,
                _ => false,
            },
            "RegExpFilter" => match self.0 {
                Blocking(_) | Whitelist(_) => true,
                _ => false,
            },
            "BlockingFilter" => {
                if let Blocking(_) = self.0 {
                    true
                } else {
                    false
                }
            }
            "WhitelistFilter" => {
                if let Whitelist(_) = self.0 {
                    true
                } else {
                    false
                }
            }
            "ContentFilter" => match self.0 {
                ElemHide(_) | ElemHideException(_) | ElemHideEmulation(_) | Snippet(_) => true,
                _ => false,
            },
            "ElemHideBase" => match self.0 {
                ElemHide(_) | ElemHideException(_) | ElemHideEmulation(_) => true,
                _ => false,
            },
            "ElemHideFilter" => {
                if let ElemHide(_) = self.0 {
                    true
                } else {
                    false
                }
            }
            "ElemHideException" => {
                if let ElemHideException(_) = self.0 {
                    true
                } else {
                    false
                }
            }
            "ElemHideEmulationFilter" => {
                if let ElemHideEmulation(_) = self.0 {
                    true
                } else {
                    false
                }
            }
            "SnippetFilter" => {
                if let Snippet(_) = self.0 {
                    true
                } else {
                    false
                }
            }
            _ => false,
        }
    }

    #[wasm_bindgen]
    pub fn normalize(text: Option<String>) -> Option<String> {
        FilterBase::normalize(text)
    }

    #[wasm_bindgen(getter)]
    pub fn csp(&self) -> JsValue {
        if let FilterEnum::Blocking(filter) = &self.0 {
            filter.csp.as_ref().map_or(JsValue::null(), JsValue::from)
        } else {
            JsValue::undefined()
        }
    }

    #[wasm_bindgen(getter)]
    pub fn reason(&self) -> Option<String> {
        if let FilterEnum::Invalid(filter) = &self.0 {
            Some(filter.reason.to_string())
        } else {
            None
        }
    }

    #[wasm_bindgen(getter)]
    pub fn text(&self) -> Option<String> {
        use FilterEnum::*;

        Some(
            match &self.0 {
                Invalid(filter) => filter.text(),
                Comment(filter) => filter.text(),
                Whitelist(filter) => filter.text(),
                Blocking(filter) => filter.text(),
                ElemHide(filter)
                | ElemHideException(filter)
                | ElemHideEmulation(filter)
                | Snippet(filter) => filter.text(),
            }
            .to_string(),
        )
    }

    #[wasm_bindgen(getter)]
    pub fn domains(&self) -> JsValue {
        self.domains_to_js(self.0.as_active_filter())
    }

    #[wasm_bindgen(getter)]
    pub fn disabled(&self) -> Option<bool> {
        self.0.as_active_filter().map(|f| f.disabled)
    }

    #[wasm_bindgen(getter, js_name = hitCount)]
    pub fn hit_count(&self) -> Option<u32> {
        self.0.as_active_filter().map(|f| f.hit_count)
    }

    #[wasm_bindgen(getter, js_name = lastHit)]
    pub fn last_hit(&self) -> Option<u32> {
        self.0.as_active_filter().map(|f| f.last_hit)
    }

    #[wasm_bindgen(getter)]
    pub fn sitekeys(&self) -> Option<Box<[JsValue]>> {
        self.0.as_active_filter().and_then(|f| {
            f.sitekeys.as_ref().map(|v| {
                v.iter()
                    .map(JsValue::from)
                    .collect::<Vec<JsValue>>()
                    .into_boxed_slice()
            })
        })
    }

    #[wasm_bindgen]
    pub fn serialize(&self) -> Option<Box<[JsValue]>> {
        self.0.as_active_filter().map(|f| {
            f.serialize()
                .into_iter()
                .map(JsValue::from)
                .collect::<Vec<JsValue>>()
                .into_boxed_slice()
        })
    }

    #[wasm_bindgen(js_name = fromObject)]
    pub fn from_object(obj: JsValue) -> Option<Filter> {
        if let Some(text) = js::Reflect::get(&obj, &JsValue::from("text"))
            .ok()
            .and_then(|v| v.as_string())
        {
            let obj_props = ObjProps::from(obj);
            let filter = FilterBase::from_text(&text, Some(obj_props));
            Some(filter)
        } else {
            None
        }
    }

    #[wasm_bindgen(getter, js_name = contentType)]
    pub fn content_type(&self) -> Option<u32> {
        self.0.as_regexp_filter().map(|f| f.content_type.bits())
    }

    #[wasm_bindgen(getter, js_name = matchCase)]
    pub fn match_case(&self) -> Option<bool> {
        self.0.as_regexp_filter().map(|f| f.match_case)
    }

    #[wasm_bindgen(getter)]
    pub fn rewrite(&self) -> JsValue {
        self.0.as_regexp_filter().map_or(JsValue::undefined(), |f| {
            f.rewrite.as_ref().map_or(JsValue::null(), JsValue::from)
        })
    }

    #[wasm_bindgen(getter)]
    pub fn regexp(&self) -> JsValue {
        self.0
            .as_regexp_filter()
            .map_or(JsValue::undefined(), |f| match f.pattern {
                Pattern::RegExp(ref s) => JsValue::from(js::RegExp::new(s, "")),
                _ => JsValue::null(),
            })
    }

    #[wasm_bindgen(js_name = rewriteUrl)]
    // XXX maybe we should return an error
    pub fn rewrite_url(&self, url: &str) -> Option<String> {
        match &self.0 {
            FilterEnum::Blocking(filter) => Some(filter.rewrite_url(url)),
            _ => None,
        }
    }

    #[wasm_bindgen(js_name = thirdParty, getter = thirdParty)]
    pub fn third_party(&self) -> JsValue {
        self.0.as_regexp_filter().map_or(JsValue::undefined(), |f| {
            f.third_party.map_or(JsValue::null(), JsValue::from_bool)
        })
    }

    #[wasm_bindgen(getter)]
    pub fn script(&self) -> Option<String> {
        if let FilterEnum::Snippet(filter) = &self.0 {
            filter.script().map(|s| s.to_string())
        } else {
            None
        }
    }

    #[wasm_bindgen(getter)]
    pub fn selector(&self) -> Option<String> {
        use FilterEnum::*;
        match &self.0 {
            ElemHide(filter) | ElemHideException(filter) | ElemHideEmulation(filter) => {
                filter.selector().map(|s| s.to_string())
            }
            _ => None,
        }
    }
}

/// Implementation not bindgen
impl Filter {
    pub fn new(obj: FilterEnum) -> Self {
        Filter(obj)
    }

    pub fn get_obj(&self) -> &FilterEnum {
        &self.0
    }

    /// Convert domains from ActiveDilter to JS Map.
    /// XXX eventually cache this
    fn domains_to_js(&self, filter: Option<&ActiveFilter>) -> JsValue {
        filter.map_or_else(JsValue::null, |f| {
            let jsmap = js::Map::new();
            if let Some(m) = f.domains() {
                m.iter().for_each(|(key, value)| {
                    jsmap.set(&JsValue::from_str(key), &JsValue::from_bool(*value));
                })
            }
            JsValue::from(jsmap)
        })
    }
}
