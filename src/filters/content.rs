use js_sys as js;
use std::sync::Arc;

use crate::url::DomainSource;

use super::base::{ActiveFilter, FilterBase, FilterExt, InvalidFilter, ObjProps, ToFilter};
use super::{Filter, FilterEnum};

#[derive(Debug)]
enum ContentFilterType {
    ElemHide,
    ElemHideException,
    ElemHideEmulation,
    Snippet,
}

#[derive(Debug)]
pub struct ContentFilter {
    parent: ActiveFilter,
    body: String,
    filter_type: ContentFilterType,
}

impl FilterExt for ContentFilter {
    fn get_type(&self) -> &'static str {
        use ContentFilterType::*;

        match self.filter_type {
            ElemHide => "elemhide",
            ElemHideException => "elemhideexception",
            ElemHideEmulation => "elemhideemulation",
            Snippet => "snippet",
        }
    }
}

impl ToFilter for ContentFilter {
    fn to_enum(self) -> FilterEnum {
        use ContentFilterType::*;

        match self.filter_type {
            ElemHide => FilterEnum::ElemHide(Arc::new(self)),
            ElemHideEmulation => FilterEnum::ElemHideEmulation(Arc::new(self)),
            ElemHideException => FilterEnum::ElemHideException(Arc::new(self)),
            Snippet => FilterEnum::Snippet(Arc::new(self)),
        }
    }
}

pub trait ElemHideExt {
    fn selector(&self) -> Option<&str>;
}
pub trait SnippetExt {
    fn script(&self) -> Option<&str>;
}

impl ElemHideExt for ContentFilter {
    fn selector(&self) -> Option<&str> {
        match self.filter_type {
            ContentFilterType::Snippet => None,
            _ => Some(&self.body),
        }
    }
}

impl SnippetExt for ContentFilter {
    fn script(&self) -> Option<&str> {
        match self.filter_type {
            ContentFilterType::Snippet => Some(&self.body),
            _ => None,
        }
    }
}

impl ContentFilter {
    /**
     * Base class for content filters
     * @param {string} text see {@link Filter Filter()}
     * @param {string} [domains] Host names or domains the filter should be
     *                           restricted to
     * @param {string} body      The body of the filter
     * @constructor
     * @augments ActiveFilter
     */
    fn new(
        filter_type: ContentFilterType,
        text: &str,
        domains: Option<DomainSource>,
        body: &str,
        obj: Option<ObjProps>,
    ) -> ContentFilter {
        ContentFilter {
            parent: ActiveFilter::new(text, domains, None, obj),
            body: body.to_string(),
            filter_type,
        }
    }

    /**
     * Creates a content filter from a pre-parsed text representation
     *
     * @param {string} text         same as in Filter()
     * @param {string} [domains]
     *   domains part of the text representation
     * @param {string} [type]
     *   rule type, either:
     *     <li>"" for an element hiding filter</li>
     *     <li>"@" for an element hiding exception filter</li>
     *     <li>"?" for an element hiding emulation filter</li>
     *     <li>"$" for a snippet filter</li>
     * @param {string} body
     *   body part of the text representation, either a CSS selector or a snippet
     *   script
     * @return {ElemHideFilter|ElemHideException|
     *          ElemHideEmulationFilter|SnippetFilter|InvalidFilter}
     */
    pub fn from_text(
        text: &str,
        domains: Option<DomainSource>,
        ftype: &str,
        body: &str,
        obj: Option<ObjProps>,
    ) -> Filter {
        if domains.is_some()
            && !domains.as_ref().unwrap().is_empty()
            && js::RegExp::new("(^|,)~?(,|$)", "")
                .test(domains.as_ref().map(|s| s.as_str()).unwrap())
        {
            InvalidFilter::new(text, "filter_invalid_domain").to_filter()
        } else {
            match ftype {
                "@" => ContentFilter::new(
                    ContentFilterType::ElemHideException,
                    text,
                    domains,
                    body,
                    obj,
                )
                .to_filter(),
                "?" | "$" => {
                    if !js::RegExp::new(",[^~][^,.]*\\.[^,]", "")
                        .test(&format!(",{}", domains.as_ref().unwrap()))
                        || format!(",{},", domains.as_ref().unwrap()).contains(",localhost,")
                    {
                        InvalidFilter::new(
                            text,
                            if ftype == "?" {
                                "filter_elemhideemulation_nodomain"
                            } else {
                                "filter_snippet_nodomain"
                            },
                        )
                        .to_filter()
                    } else if ftype == "?" {
                        ContentFilter::new(
                            ContentFilterType::ElemHideEmulation,
                            text,
                            domains,
                            body,
                            obj,
                        )
                        .to_filter()
                    } else {
                        ContentFilter::new(ContentFilterType::Snippet, text, domains, body, obj)
                            .to_filter()
                    }
                }
                _ => ContentFilter::new(ContentFilterType::ElemHide, text, domains, body, obj)
                    .to_filter(),
            }
        }
    }
}

impl AsRef<ActiveFilter> for ContentFilter {
    fn as_ref(&self) -> &ActiveFilter {
        &self.parent
    }
}

impl AsRef<FilterBase> for ContentFilter {
    fn as_ref(&self) -> &FilterBase {
        self.parent.as_ref()
    }
}
