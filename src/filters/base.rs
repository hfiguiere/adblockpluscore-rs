#[cfg(target_arch = "wasm32")]
use js_sys as js;
use std::collections::HashMap;
use std::fmt;
use std::sync::{Arc, Mutex};

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

use super::content::ContentFilter;
use super::regexp::RegExpFilter;
use crate::filters::{Filter, FilterEnum};
#[cfg(target_arch = "wasm32")]
use crate::js_helper::{get_int_property_string, push_pair, replace_regexp};
use crate::url;
use crate::url::DomainSource;

lazy_static! {
    static ref KNOWN_FILTERS: Mutex<HashMap<String, Filter>> =
        Mutex::new(HashMap::new());

    // XXX it seems that js::RegExp isn't Sync
    // static ref CONTENT_REGEXP: js::RegExp =
    //     js::RegExp::new("^([^/*|@\"!]*?)#([@?$])?#(.+)$", "");
}

const CONTENT_REGEXP: &str = "^([^/*|@\"!]*?)#([@?$])?#(.+)$";
pub const OPTIONS_REGEXP: &str = "\\$(~?[\\w-]+(?:=[^,]*)?(?:,~?[\\w-]+(?:=[^,]*)?)*)$";

/// Object properties from serialization
pub struct ObjProps(Vec<(String, String)>);

impl From<JsValue> for ObjProps {
    /// Convert from a JsValue object
    fn from(obj: JsValue) -> ObjProps {
        let mut vec = vec![];
        if let Ok(iterator) = js::try_iter(&js::Object::entries(&js::Object::from(obj))) {
            if let Some(iterator) = iterator {
                for entry in iterator {
                    if let Ok(e) = entry {
                        push_pair(&mut vec, &e);
                    }
                }
            }
        }
        ObjProps(vec)
    }
}

pub trait Text {
    /// The filter text.
    fn text(&self) -> &str;
}

pub trait Serialize {
    fn serialize(&self) -> Vec<String>;
}

/// Exported Filter API
pub trait FilterExt: Text {
    /// The type string for the filter.
    fn get_type(&self) -> &'static str;
}

/// The trait Text can be implemented using AsRef<>
impl<T: AsRef<FilterBase>> Text for T {
    /// The filter text.
    fn text(&self) -> &str {
        &self.as_ref().text
    }
}

/// To convert a filter object to a filter
pub trait ToFilter: std::marker::Sized {
    fn to_enum(self) -> FilterEnum;
    fn to_filter(self) -> Filter {
        Filter::new(self.to_enum())
    }
}

#[derive(Debug)]
pub struct FilterBase {
    text: String,
}

impl FilterBase {
    pub fn new(text: &str) -> FilterBase {
        FilterBase {
            text: text.to_string(),
        }
    }

    pub fn from_text(text: &str, obj: Option<ObjProps>) -> Filter {
        if let Some(filter) = KNOWN_FILTERS.lock().unwrap().get(text) {
            return filter.clone();
        }

        let filter = if text.chars().nth(0) == Some('!') {
            CommentFilter::new(text).to_filter()
        } else {
            let m = if text.contains('#') {
                // XXX use a shared RegExp
                let re = js::RegExp::new(CONTENT_REGEXP, "");
                re.exec(&text)
            } else {
                None
            };
            if let Some(m) = m {
                let domains = m.get(1).as_string();
                let ftype = m.get(2).as_string();
                let body = m.get(3).as_string();
                ContentFilter::from_text(
                    text,
                    domains.map(|s| DomainSource::Comma(s)),
                    &ftype.unwrap_or_default(),
                    &body.unwrap_or_default(),
                    obj,
                )
            } else {
                RegExpFilter::from_text(text, obj)
            }
        };
        KNOWN_FILTERS
            .lock()
            .unwrap()
            .insert(text.to_string(), filter.clone());
        filter
    }

    pub fn normalize(text: Option<String>) -> Option<String> {
        if text.is_none() {
            return text;
        }

        let mut text = text.unwrap();

        // Remove line breaks, tabs etc
        let re = js::RegExp::new("[^\\S ]+", "g");
        text = replace_regexp(&text, &re, "");

        // Don't remove spaces inside comments
        if js::RegExp::new("^ *!", "").test(&text) {
            return Some(text.trim().to_string());
        }

        // Special treatment for content filters, right side is allowed to
        // contain spaces
        if js::RegExp::new(CONTENT_REGEXP, "").test(&text) {
            let re = js::RegExp::new("^(.*?)(#[@?$]?#?)(.*)$", "");
            if let Some(m) = re.exec(&text) {
                let domains = m.get(1).as_string();
                let separator = m.get(2).as_string();
                let body = m.get(3).as_string();
                return Some(
                    domains.unwrap().replace(' ', "") + &separator.unwrap() + body.unwrap().trim(),
                );
            }
        }

        // For most regexp filters we strip all spaces, but $csp
        // filter options are allowed to contain single (non trailing)
        // spaces.
        let stripped_text = text.replace(' ', "");
        if !stripped_text.contains('$') || !js::RegExp::new("\\bcsp=", "i").test(&stripped_text) {
            return Some(stripped_text);
        }

        let re = js::RegExp::new(OPTIONS_REGEXP, "");
        let options_match = re.exec(&stripped_text);
        if options_match.is_none() {
            return Some(stripped_text);
        }
        let options_match = options_match.unwrap();

        // For $csp filters we must first separate out the options
        // part of the text, being careful to preserve its spaces.
        let index = get_int_property_string(&options_match, "index").unwrap_or(0) as usize;
        let before_options = stripped_text.get(0..index).unwrap();
        let mut stripped_dollar_index: Option<usize>;
        let mut dollar_index: Option<usize>;
        let (_, mut before_options_slice) = before_options.split_at(0);
        let (_, mut text_slice) = text.split_at(0);
        loop {
            stripped_dollar_index = before_options_slice.find('$');
            let (_, s) = before_options_slice.split_at(stripped_dollar_index.map_or(0, |v| v + 1));
            before_options_slice = s;

            dollar_index = text_slice.find('$');
            let (_, s) = text_slice.split_at(dollar_index.map_or(0, |v| v + 1));
            text_slice = s;

            if stripped_dollar_index.is_none() {
                break;
            }
        }

        let options_text = text_slice;

        // Then we can normalize spaces in the options part safely
        let options = options_text.split(',');
        let mut new_options: Vec<String> = vec![];
        let csp_re = js::RegExp::new("^ *c *s *p *=", "i");
        let ws_re = js::RegExp::new(" +", "");
        for option in options {
            let csp_match = csp_re.exec(option);
            if let Some(csp_match) = csp_match {
                let m0 = csp_match.get(0).as_string().unwrap();
                let s = replace_regexp(option.get(m0.len()..).unwrap().trim(), &ws_re, " ");
                new_options.push(m0.replace(' ', "") + &s);
            } else {
                new_options.push(option.replace(' ', ""));
            }
        }

        Some(before_options.to_string() + "$" + &new_options.join(","))
    }
}

impl Serialize for FilterBase {
    fn serialize(&self) -> Vec<String> {
        let mut serialization = vec![];
        serialization.push("[Filter]".to_string());
        serialization.push("text=".to_string() + &self.text);
        serialization
    }
}

pub struct InvalidFilter {
    parent: FilterBase,
    pub reason: String,
}

impl InvalidFilter {
    pub fn new(text: &str, reason: &str) -> Self {
        InvalidFilter {
            parent: FilterBase::new(text),
            reason: reason.to_string(),
        }
    }
}

impl Serialize for InvalidFilter {
    fn serialize(&self) -> Vec<String> {
        vec![]
    }
}

impl FilterExt for InvalidFilter {
    fn get_type(&self) -> &'static str {
        "invalid"
    }
}

impl AsRef<FilterBase> for InvalidFilter {
    fn as_ref(&self) -> &FilterBase {
        &self.parent
    }
}

impl fmt::Debug for InvalidFilter {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "InvalidFilter {{ reason: {}, text: {} }}",
            &self.reason,
            &self.text()
        )
    }
}

impl ToFilter for InvalidFilter {
    fn to_enum(self) -> FilterEnum {
        FilterEnum::Invalid(Arc::new(self))
    }
    fn to_filter(self) -> Filter {
        Filter::new(self.to_enum())
    }
}

#[derive(Debug)]
pub struct CommentFilter {
    parent: FilterBase,
}

impl CommentFilter {
    fn new(text: &str) -> Self {
        CommentFilter {
            parent: FilterBase::new(text),
        }
    }
}

impl Serialize for CommentFilter {
    fn serialize(&self) -> Vec<String> {
        vec![]
    }
}

impl AsRef<FilterBase> for CommentFilter {
    fn as_ref(&self) -> &FilterBase {
        &self.parent
    }
}

impl ToFilter for CommentFilter {
    fn to_enum(self) -> FilterEnum {
        FilterEnum::Comment(Arc::new(self))
    }
    fn to_filter(self) -> Filter {
        Filter::new(self.to_enum())
    }
}

impl FilterExt for CommentFilter {
    fn get_type(&self) -> &'static str {
        "comment"
    }
}

#[derive(Debug)]
pub struct ActiveFilter {
    parent: FilterBase,
    pub domain_source: Option<DomainSource>,
    pub sitekeys: Option<Vec<String>>,
    pub disabled: bool,
    pub hit_count: u32,
    pub last_hit: u32,
}

impl Serialize for ActiveFilter {
    fn serialize(&self) -> Vec<String> {
        let mut serialize = self.parent.serialize();
        if self.disabled {
            serialize.push("disabled=true".to_string());
        }
        if self.hit_count > 0 {
            serialize.push(format!("hitCount={}", self.hit_count));
        }
        if self.last_hit > 0 {
            serialize.push(format!("lastHit={}", self.last_hit));
        }
        serialize
    }
}

impl AsRef<FilterBase> for ActiveFilter {
    fn as_ref(&self) -> &FilterBase {
        &self.parent
    }
}

impl ActiveFilter {
    pub fn new(
        text: &str,
        domains: Option<DomainSource>,
        sitekeys: Option<&String>,
        obj: Option<ObjProps>,
    ) -> Self {
        let mut hit_count = 0u32;
        let mut last_hit = 0u32;
        let mut disabled = false;
        if let Some(obj) = obj {
            for prop in obj.0 {
                match prop.0.as_str() {
                    "disabled" => {
                        if &prop.1 == "true" {
                            disabled = true;
                        }
                    }
                    "hitCount" => {
                        if let Ok(v) = u32::from_str_radix(&prop.1, 10) {
                            hit_count = v;
                        }
                    }
                    "lastHit" => {
                        if let Ok(v) = u32::from_str_radix(&prop.1, 10) {
                            last_hit = v;
                        }
                    }
                    _ => continue,
                }
            }
        }
        let sitekeys =
            sitekeys.map(|v| v.split('|').map(|s| s.to_string()).collect::<Vec<String>>());
        ActiveFilter {
            parent: FilterBase::new(text),
            domain_source: domains.map(|s| s.to_lowercase()),
            sitekeys,
            disabled,
            hit_count,
            last_hit,
        }
    }

    /**
     * Map containing domains that this filter should match on/not match
     * on or null if the filter should match on all domains
     * @type {?Map.<string,boolean>}
     */
    pub fn domains(&self) -> Option<url::DomainsInclude> {
        if self.domain_source.is_none() || self.domain_source.as_ref().unwrap().is_empty() {
            return None;
        }

        // XXX this should be cached because domain_source is immutable
        url::parse_domains(&self.domain_source.as_ref().unwrap())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[wasm_bindgen_test]
    fn test_from_text() {
        let f = RegExpFilter::from_text(
            "bla$match-case,csp=first csp,script,other,third-party,domain=FOO.cOm,sitekey=foo",
            None,
        );
        assert_eq!(f.get_type(), Some("blocking".to_string()));
        if let FilterEnum::Blocking(bf) = f.get_obj() {
            assert_eq!(bf.csp, Some("first csp".to_string()));
        } else {
            assert!(false);
        }
        assert_eq!(f.csp().as_string(), Some("first csp".to_string()));
    }

    #[wasm_bindgen_test]
    fn test_normalize() {
        let normalized = Filter::normalize(Some(
            "    b$l 	 a$sitekey=  foo  ,domain= do main.com |foo   .com,c sp= c   s p  "
                .to_string(),
        ));
        assert_eq!(
            normalized.unwrap(),
            "b$la$sitekey=foo,domain=domain.com|foo.com,csp=c s p"
        );
    }

    #[wasm_bindgen_test]
    fn test_from_text_comment() {
        let f = FilterBase::from_text("!asdf", None);
        assert_eq!(f.get_type(), Some("comment".to_string()));
    }
}
