use std::collections::HashMap;
use std::str::FromStr;
use std::sync::Arc;

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

use js_sys as js;

use crate::content_types::ContentTypes;
#[cfg(target_arch = "wasm32")]
use crate::js_helper::{get_int_property_string, insert_pair};
use crate::url::DomainSource;

use super::base::{
    ActiveFilter, FilterBase, FilterExt, InvalidFilter, ObjProps, ToFilter, OPTIONS_REGEXP,
};
use super::{Filter, FilterEnum};

/// This is a hack: reimport the RegExp constructor so that
/// it returns an error from the expection throwing.
#[wasm_bindgen(module = "/js/glue.js")]
extern "C" {
    #[wasm_bindgen(catch)]
    pub fn new_regexp_try(pattern: &str, flags: &str) -> Result<js::RegExp, JsValue>;
}

const RESOURCES: &str = include_str!("../../data/resources.json");

// XXX make this abstracted to not rely on js
fn load_resources(resources: &str) -> HashMap<String, String> {
    let r = js::JSON::parse(resources);
    if let Ok(o) = r {
        let mut hash = HashMap::new();
        if let Ok(iterator) = js::try_iter(&js::Object::entries(&js::Object::from(o))) {
            if let Some(iterator) = iterator {
                for entry in iterator {
                    if let Ok(e) = entry {
                        insert_pair(&mut hash, &e);
                    }
                }
            }
        }
        hash
    } else {
        HashMap::new()
    }
}

lazy_static! {
    static ref RESOURCE_MAP: HashMap<String, String> = load_resources(RESOURCES);
}

trait RemoveWildcards {
    fn remove_wildcards(&self) -> String;
}

impl RemoveWildcards for str {
    fn remove_wildcards(&self) -> String {
        let mut b: usize = 0;
        let mut e: usize = 0;
        for c in self.chars() {
            if c != '*' {
                break;
            }
            b += 1;
        }
        for c in self.chars().rev() {
            if c != '*' {
                break;
            }
            e += 1;
        }
        self.get(b..self.len() - e).unwrap().to_string()
    }
}

// XXX flags = /i
const INVALID_CSP_REGEXP: &str =
    "(;|^) ?(base-uri|referrer|report-to|report-uri|upgrade-insecure-requests)\\b";

#[derive(Debug)]
pub enum Pattern {
    Pat(String),
    RegExp(String),
}

#[derive(Debug)]
pub struct RegExpFilter {
    parent: ActiveFilter,
    pub pattern: Pattern,
    pub content_type: ContentTypes,
    pub match_case: bool,
    pub third_party: Option<bool>,
    sitekey_source: Option<String>,
    pub rewrite: Option<String>,
}

impl AsRef<ActiveFilter> for RegExpFilter {
    fn as_ref(&self) -> &ActiveFilter {
        &self.parent
    }
}

impl AsRef<FilterBase> for RegExpFilter {
    fn as_ref(&self) -> &FilterBase {
        self.parent.as_ref()
    }
}

impl RegExpFilter {
    const CONTENT_TYPE: ContentTypes = ContentTypes::RESOURCE_TYPES;

    /**
     * Abstract base class for RegExp-based filters
     * @param {string} text see {@link Filter Filter()}
     * @param {string} regexpSource
     *   filter part that the regular expression should be build from
     * @param {number} [contentType]
     *   Content types the filter applies to, combination of values from
     *   RegExpFilter.typeMap
     * @param {boolean} [matchCase]
     *   Defines whether the filter should distinguish between lower and upper case
     *   letters
     * @param {string} [domains]
     *   Domains that the filter is restricted to, e.g. "foo.com|bar.com|~baz.com"
     * @param {boolean} [thirdParty]
     *   Defines whether the filter should apply to third-party or first-party
     *   content only
     * @param {string} [sitekeys]
     *   Public keys of websites that this filter should apply to
     * @param {?string} [rewrite]
     *   The name of the internal resource to which to rewrite the
     *   URL. e.g. if the value of the <code>$rewrite</code> option is
     *   <code>abp-resource:blank-html</code>, this should be
     *   <code>blank-html</code>.
     * @constructor
     * @augments ActiveFilter
     */
    pub fn new(
        text: &str,
        regexp_source: &str,
        content_type: Option<ContentTypes>,
        match_case: Option<bool>,
        domains: Option<DomainSource>,
        third_party: Option<bool>,
        sitekey_source: Option<String>,
        rewrite: Option<String>,
        obj: Option<ObjProps>,
    ) -> Result<RegExpFilter, String> {
        // XXX
        let match_case = match_case.unwrap_or(false);
        let content_type = content_type.unwrap_or(ContentTypes::RESOURCE_TYPES);
        let mut regexp_source = if match_case {
            regexp_source.to_lowercase()
        } else {
            regexp_source.to_string()
        };
        let len = regexp_source.len();
        let pattern: Pattern = if len >= 2
            && regexp_source.chars().nth(0) == Some('/')
            && regexp_source.chars().nth(len - 1) == Some('/')
        {
            // XXX can't seems to be able to put a RegExp in there.
            let re_source = regexp_source.get(1..len - 1).unwrap();
            let re = new_regexp_try(re_source, "");
            if re.is_err() {
                return Err("filter_invalid_regexp".to_string());
            }
            Pattern::RegExp(re_source.to_string())
        } else {
            regexp_source = regexp_source.remove_wildcards();
            Pattern::Pat(regexp_source)
        };
        Ok(RegExpFilter {
            parent: ActiveFilter::new(text, domains, sitekey_source.as_ref(), obj),
            pattern,
            content_type,
            match_case,
            third_party,
            sitekey_source,
            rewrite,
        })
    }

    // XXX
    pub fn from_text(text: &str, obj: Option<ObjProps>) -> Filter {
        let mut blocking: bool = true;
        let mut current_text = text;

        if current_text.starts_with("@@") {
            blocking = false;
            current_text = current_text.get(2..).unwrap();
        }

        let mut content_type: Option<ContentTypes> = None;
        let mut match_case: Option<bool> = None;
        let mut domains: Option<DomainSource> = None;
        let mut sitekeys: Option<String> = None;
        let mut third_party: Option<bool> = None;
        let mut csp: Option<String> = None;
        let mut rewrite: Option<String> = None;
        let options: Vec<&str>;
        let input = current_text;
        let match_ = if text.contains('$') {
            // TODO we shouldn't need to recreate it all the time
            js::RegExp::new(OPTIONS_REGEXP, "").exec(current_text)
        } else {
            None
        };
        if let Some(match_) = match_ {
            if let Some(o) = match_.get(1).as_string() {
                options = o.split(',').collect();
                let index = get_int_property_string(&match_, "index").unwrap_or(0) as usize;
                current_text = input.get(..index).unwrap();
                for option_text in options {
                    let mut value: Option<&str> = None;
                    let mut option = if let Some(separator_index) = option_text.find('=') {
                        value = option_text.get(separator_index + 1..);
                        option_text.get(0..separator_index).unwrap()
                    } else {
                        option_text
                    };
                    let inverse = option.chars().nth(0) == Some('~');
                    if inverse {
                        option = option.get(1..).unwrap();
                    }

                    let ct = option.replace("-", "_").to_uppercase();
                    if let Ok(ctype) = ContentTypes::from_str(&ct) {
                        if inverse {
                            content_type = content_type
                                .map(|c| c & !ctype)
                                .or(Some(RegExpFilter::CONTENT_TYPE));
                        } else {
                            content_type = content_type.map(|c| c | ctype).or(Some(ctype));
                            if ctype == ContentTypes::CSP {
                                if blocking && value.is_none() {
                                    return InvalidFilter::new(text, "filter_invalid_csp")
                                        .to_filter();
                                }
                                csp = value.map(|s| s.to_string());
                            }
                        }
                    } else {
                        match option.to_lowercase().as_str() {
                            "match-case" => match_case = Some(!inverse),
                            "domain" => {
                                if value.is_none() {
                                    return InvalidFilter::new(text, "filter_unknown_opton")
                                        .to_filter();
                                }
                                domains = value.map(|s| DomainSource::Pipe(s.to_string()));
                            }
                            "third-party" => third_party = Some(!inverse),
                            "sitekey" => {
                                if let Some(v) = value {
                                    sitekeys = Some(v.to_uppercase());
                                } else {
                                    return InvalidFilter::new(text, "filter_unknown_option")
                                        .to_filter();
                                }
                            }
                            "rewrite" => {
                                if let Some(v) = value {
                                    if !v.starts_with("abp-resource:") {
                                        return InvalidFilter::new(text, "filter_invalid_rewrite")
                                            .to_filter();
                                    }
                                    rewrite = v.get("abp-resource:".len()..).map(|s| s.to_string());
                                } else {
                                    return InvalidFilter::new(text, "filter_unknown_option")
                                        .to_filter();
                                }
                            }
                            _ => {
                                return InvalidFilter::new(text, "filter_unknown_option")
                                    .to_filter()
                            }
                        }
                    }
                }
            }
        }

        // XXX JS code has a try here
        if blocking {
            if csp.is_some() && js::RegExp::new(INVALID_CSP_REGEXP, "").test(&csp.as_ref().unwrap())
            {
                return InvalidFilter::new(text, "filter_invalid_csp").to_filter();
            }
            if rewrite.is_some() {
                if current_text.chars().nth(0) == Some('|')
                    && current_text.chars().nth(1) == Some('|')
                {
                    if domains.is_none() && third_party != Some(false) {
                        return InvalidFilter::new(text, "filter_invalid_rewrite").to_filter();
                    }
                } else if current_text.chars().nth(0) == Some('*') {
                    if domains.is_none() {
                        return InvalidFilter::new(text, "filter_invalid_rewrite").to_filter();
                    }
                } else {
                    return InvalidFilter::new(text, "filter_invalid_rewrite").to_filter();
                }
            }

            return match BlockingFilter::new(
                text,
                current_text,
                content_type,
                match_case,
                domains,
                third_party,
                sitekeys,
                rewrite,
                csp,
                obj,
            ) {
                Ok(filter) => filter.to_filter(),
                Err(err) => InvalidFilter::new(text, &err).to_filter(),
            };
        }

        match WhitelistFilter::new(
            text,
            current_text,
            content_type,
            match_case,
            domains,
            third_party,
            sitekeys,
            obj,
        ) {
            Ok(filter) => filter.to_filter(),
            Err(err) => InvalidFilter::new(text, &err).to_filter(),
        }
    }

    pub fn matches(&self) -> bool {
        false // XXX actually do the work
    }

    /**
     * Checks whether this filter has only a URL pattern and no content type,
     * third-party flag, domains, or sitekeys.
     * @returns {boolean}
     */
    pub fn is_location_only(&self) -> bool {
        self.content_type == RegExpFilter::CONTENT_TYPE
            && self.third_party.is_none()
            && self.parent.domain_source.is_none()
            && self.sitekey_source.is_none()
            && self.parent.domains().is_none()
            && self.parent.sitekeys.is_none()
    }
}

#[derive(Debug)]
pub struct BlockingFilter {
    parent: RegExpFilter,
    pub csp: Option<String>,
}

impl BlockingFilter {
    /**
     * Class for blocking filters
     * @param {string} text see {@link Filter Filter()}
     * @param {string} regexpSource see {@link RegExpFilter RegExpFilter()}
     * @param {number} [contentType] see {@link RegExpFilter RegExpFilter()}
     * @param {boolean} [matchCase] see {@link RegExpFilter RegExpFilter()}
     * @param {string} [domains] see {@link RegExpFilter RegExpFilter()}
     * @param {boolean} [thirdParty] see {@link RegExpFilter RegExpFilter()}
     * @param {string} [sitekeys] see {@link RegExpFilter RegExpFilter()}
     * @param {?string} [rewrite]
     *   The name of the internal resource to which to rewrite the
     *   URL. e.g. if the value of the <code>$rewrite</code> option is
     *   <code>abp-resource:blank-html</code>, this should be
     *   <code>blank-html</code>.
     * @param {string} [csp]
     *   Content Security Policy to inject when the filter matches
     * @constructor
     * @augments RegExpFilter
     */
    fn new(
        text: &str,
        regexp_source: &str,
        content_type: Option<ContentTypes>,
        match_case: Option<bool>,
        domains: Option<DomainSource>,
        third_party: Option<bool>,
        sitekeys: Option<String>,
        rewrite: Option<String>,
        csp: Option<String>,
        obj: Option<ObjProps>,
    ) -> Result<BlockingFilter, String> {
        match RegExpFilter::new(
            text,
            regexp_source,
            content_type,
            match_case,
            domains,
            third_party,
            sitekeys,
            rewrite,
            obj,
        ) {
            Ok(parent) => Ok(BlockingFilter { parent, csp }),
            Err(err) => Err(err),
        }
    }

    /**
     * Rewrites an URL.
     * @param {string} url the URL to rewrite
     * @return {string} the rewritten URL, or the original in case of failure
     */
    pub fn rewrite_url(&self, url: &str) -> String {
        if self.parent.rewrite.is_none() {
            return url.to_string();
        }
        let rewrite = self.parent.rewrite.as_ref().unwrap();
        if let Some(rewritten) = RESOURCE_MAP.get(rewrite) {
            rewritten.to_string()
        } else {
            url.to_string()
        }
    }
}

impl ToFilter for BlockingFilter {
    fn to_enum(self) -> FilterEnum {
        FilterEnum::Blocking(Arc::new(self))
    }
    fn to_filter(self) -> Filter {
        Filter::new(self.to_enum())
    }
}

impl FilterExt for BlockingFilter {
    fn get_type(&self) -> &'static str {
        "blocking"
    }
}

impl AsRef<RegExpFilter> for BlockingFilter {
    fn as_ref(&self) -> &RegExpFilter {
        &self.parent
    }
}

impl AsRef<ActiveFilter> for BlockingFilter {
    fn as_ref(&self) -> &ActiveFilter {
        &self.parent.as_ref()
    }
}

impl AsRef<FilterBase> for BlockingFilter {
    fn as_ref(&self) -> &FilterBase {
        &self.parent.as_ref()
    }
}

#[derive(Debug)]
pub struct WhitelistFilter {
    parent: RegExpFilter,
}

impl WhitelistFilter {
    /**
     * Class for whitelist filters
     * @param {string} text see {@link Filter Filter()}
     * @param {string} regexpSource see {@link RegExpFilter RegExpFilter()}
     * @param {number} [contentType] see {@link RegExpFilter RegExpFilter()}
     * @param {boolean} [matchCase] see {@link RegExpFilter RegExpFilter()}
     * @param {string} [domains] see {@link RegExpFilter RegExpFilter()}
     * @param {boolean} [thirdParty] see {@link RegExpFilter RegExpFilter()}
     * @param {string} [sitekeys] see {@link RegExpFilter RegExpFilter()}
     * @constructor
     * @augments RegExpFilter
     */
    fn new(
        text: &str,
        regexp_source: &str,
        content_type: Option<ContentTypes>,
        match_case: Option<bool>,
        domains: Option<DomainSource>,
        third_party: Option<bool>,
        sitekeys: Option<String>,
        obj: Option<ObjProps>,
    ) -> Result<Self, String> {
        match RegExpFilter::new(
            text,
            regexp_source,
            content_type,
            match_case,
            domains,
            third_party,
            sitekeys,
            None,
            obj,
        ) {
            Ok(parent) => Ok(WhitelistFilter { parent }),
            Err(err) => Err(err),
        }
    }
}

impl ToFilter for WhitelistFilter {
    fn to_enum(self) -> FilterEnum {
        FilterEnum::Whitelist(Arc::new(self))
    }
    fn to_filter(self) -> Filter {
        Filter::new(self.to_enum())
    }
}

impl FilterExt for WhitelistFilter {
    fn get_type(&self) -> &'static str {
        "whitelist"
    }
}

impl AsRef<RegExpFilter> for WhitelistFilter {
    fn as_ref(&self) -> &RegExpFilter {
        &self.parent
    }
}

impl AsRef<ActiveFilter> for WhitelistFilter {
    fn as_ref(&self) -> &ActiveFilter {
        self.parent.as_ref()
    }
}

impl AsRef<FilterBase> for WhitelistFilter {
    fn as_ref(&self) -> &FilterBase {
        self.parent.as_ref()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[wasm_bindgen_test]
    fn test_resources() {
        let r = load_resources(RESOURCES);

        assert!(!r.is_empty());

        let v = r.get("blank-text");
        assert!(v.is_some());
        let v = v.unwrap();
        assert_eq!(v, "data:text/plain,");
    }
}
