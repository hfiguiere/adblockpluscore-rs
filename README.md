adblockpluscore-rs

An experiment to port adblockpluscore to Rust and compile it to WASM
for use into a WebExtension.

Prerequisite
============

You need:
- Rust with `wasm32-unknown-unknown` target support. If you use rustup,
  you have nothing to do.
- `[wasm-pack](https://github.com/rustwasm/wasm-pack)`


Building
========

### 🛠️ Build with `wasm-pack build`

```
wasm-pack build
```

### 🔬 Test in Headless Browsers with `wasm-pack test`

```
wasm-pack test --headless --firefox
```

